<?php 
	//require_once '../autoload.php';
	namespace models;

	use database\Database;
	//namespace models;
	//use PDO;
	
	Class Usuario {
		
		public function save($nome, $telefone, $email) {
			$db = new Database;
			$db = $db->connect();

			$query = $db->prepare("INSERT INTO cadastro (nome,telefone,email) VALUES (':nome',':telefone',':email')");
			$query->bindParam(':nome', $nome);
			$query->bindParam(':telefone', $telefone);
			$query->bindParam(':email', $email);
			$query->execute();
			
			return $query;
		}
	}
?>
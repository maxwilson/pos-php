<?php 
namespace classes;
use PDO;

class Cadastro {
	
	protected $nome;
	protected $telefone;
	protected $email;

	public function __construct($nome='',$telefone='',$email='')
	{
		$this->setNome($nome);
		$this->setTelefone($telefone);
		$this->setEmail($email);
	}
	

	/**
	 * Get the value of nome
	 */ 
	public function getNome()
	{
		return $this->nome;
	}

	/**
	 * Set the value of nome
	 *
	 * @return  self
	 */ 
	public function setNome($nome)
	{
		$this->nome = $nome;

		return $this;
	}

	/**
	 * Get the value of telefone
	 */ 
	public function getTelefone()
	{
		return $this->telefone;
	}

	/**
	 * Set the value of telefone
	 *
	 * @return  self
	 */ 
	public function setTelefone($telefone)
	{
		$this->telefone = $telefone;

		return $this;
	}

	/**
	 * Get the value of email
	 */ 
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Set the value of email
	 *
	 * @return  self
	 */ 
	public function setEmail($email)
	{
		$this->email = $email;

		return $this;
	}

	public function exibir() {
		$db = new PDO('mysql:host=localhost;dbname=sistema','root','root');

		$query = $db->query('SELECT * FROM cadastro');
		
		if(isset($query)){
			echo "<a href='./pages/index.html'>Cadastrar novo</a><br/>";
			foreach($query as $linha) {
	
				echo "<p>".$linha['codigo']." | ".$linha['nome']." | ".$linha['telefone']." | ".$linha['email']."</P>";
			}
		}
	}

	public function inserir() {
		$db = new PDO('mysql:host=localhost;dbname=sistema','root','root');
		
		$nome = $this->getNome();
		$telefone = $this->getTelefone();
		$email = $this->getEmail();
	
		$query = $db->prepare("INSERT INTO cadastro (`nome`,`telefone`,`email`) VALUES (:nome,:telefone,:email);");
		
		$query->bindParam(':nome', $nome,PDO::PARAM_STR);
		$query->bindParam(':telefone', $telefone,PDO::PARAM_STR);
		$query->bindParam(':email', $email,PDO::PARAM_STR);
		
		
		$query->execute();
	}
}


?>
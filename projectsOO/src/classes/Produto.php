<?php 

class Produto 
{
	private $codigo;
	private $descricao;
	private $preco;
	private $quantidade;

	public function __construct($codigo,$descricao,$preco,$quantidade)
	{
		$this->codigo = $codigo;
		$this->descricao = $descricao;
		$this->preco = $preco;
		$this->quantidade = $quantidade;
	}

	public function imprimirProduto() 
	{
		echo "Código: {$this->codigo} <br/>";
		echo "Descrição: {$this->descricao}<br/>";
	}

	public function __destruct()
	{
		echo "<p>Encerrado</p>";
	}
}
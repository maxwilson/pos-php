<?php
require_once './classes/Produto.php';

$codigo = "001";
$descricao = "Produto A";
$preco = 10;
$quantidade = 5;

$produto = new Produto($codigo, $descricao, $preco, $quantidade);

$produto->imprimirProduto();

$produto = null;
<?php

namespace Classes;

	abstract class Conta 
	{
		protected $saldo;

		public function __construct($saldo=0)
		{
			$this->setSaldo($saldo);
		}

		/**
		 * Get the value of saldo
		 */ 
		public function getSaldo()
		{
			return $this->saldo;
		}

		/**
		 * Set the value of saldo
		 *
		 * @return  self
		 */ 
		public function setSaldo($saldo)
		{
			$this->saldo = $saldo;

			return $this;
		}

		final public function deposita($valor)
		{
			$this->setSaldo($this->getSaldo() + $valor);
		}

		public function saca($valor)
		{
			if($this->getSaldo() >= $valor) {
				$this->setSaldo($this->getSaldo() - $valor);
			}else{
				echo "Saldo Insuficente";
			}
		}

		public function imprimeExtrato()
		{
			$saldo = number_format($this->getSaldo(),2,",",".");

			echo "<p><strong>Extrato:</strong> R$ {$saldo}</p>";
		}
	}

?>
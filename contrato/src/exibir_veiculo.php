<?php 

	require_once '../autoload.php';

	use classes\Veiculo;

	$veiculo = new Veiculo;

	$veiculo->setNome($_POST['nome']);
	
	if($_POST['modelo'] === '1'){
		$veiculo->setModelo('carro');
	}else{
		$veiculo->setModelo('moto');
	}

	$veiculo->setMarca($_POST['marca']);

	$veiculo->exibeNome();
	$veiculo->exibeMarca();
	$veiculo->exibeModelo();
	
?>
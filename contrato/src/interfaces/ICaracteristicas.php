<?php 

namespace interfaces;

interface ICaracteristicas{
	function exibirNome();
	function exibirModelo();
	function exibirMarca();
}

?>
<?php 

	require_once '../autoload.php';

	use classes\Animal;
	use classes\Carnivoro;
	use classes\Herbivoro;

	$herbivoro = new Herbivoro;
	$carnivoro = new Carnivoro;

	$herbivoro->habitoAlimentar();
	$carnivoro->habitoAlimentar();
?>
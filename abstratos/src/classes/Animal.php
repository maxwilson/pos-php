<?php 

namespace classes;

abstract class Animal {
	protected $come;
	
	public function __construct($come='')
	{
		$this->setCome($come);
	}
	/**
	 * Get the value of come
	 */ 
	public function getCome()
	{
		return $this->come;
	}

	/**
	 * Set the value of come
	 *
	 * @return  self
	 */ 
	public function setCome($come)
	{
		$this->come = $come;

		return $this;
	}

	public function habitoAlimentar() {
		echo "<p>{$this->getCome()}</p>";
	}
}
?>